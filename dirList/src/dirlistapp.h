#ifndef DIRLISTAPP_H
#define DIRLISTAPP_H

#include <QCoreApplication>
#include <QTextStream>

class DirListApp : public QCoreApplication
{
    Q_OBJECT

private:
    const int argumentCount = 2 + 1;
    const int argumentIdx_path = 1;
    const int argumentIdx_elementCount = 2;

    QTextStream consoleOutput;
    QString path;
    uint elementCount;

    DirListApp() = delete;

    void parseArguments();
    void listElements();

public:
    DirListApp(int argc, char *argv[]);

signals:
    void appStarted();

private slots:
    void runApp();
};

#endif // DIRLISTAPP_H
