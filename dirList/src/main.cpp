#include "dirlistapp.h"

int main(int argc, char *argv[])
{
    DirListApp a(argc, argv);

    return a.exec();
}
