#include "dirlistapp.h"

#include <QDir>
#include <QDirIterator>
#include <QFile>
#include <QDateTime>

DirListApp::DirListApp(int argc, char *argv[]) : QCoreApplication(argc, argv), consoleOutput(stdout)
{
    elementCount = 0;

    QCoreApplication::setApplicationName("dirList");
    QCoreApplication::setApplicationVersion("1.0");

    connect(this, SIGNAL(appStarted()), this, SLOT(runApp()), Qt::QueuedConnection);
    emit appStarted();
}

void DirListApp::runApp()
{
    parseArguments();
    listElements();
    exit();
}

void DirListApp::parseArguments()
{
    if(this->arguments().size() != argumentCount)
    {
        qFatal("Error: Exactly two arguments are needed! (try -h or --help)");
    }

    path = this->arguments().at(argumentIdx_path);

    if(!QDir(path).exists())
    {
        qFatal("Error: The path given is invalid!");
    }

    bool elementCountParsedOk = false;
    elementCount = this->arguments().at(argumentIdx_elementCount).toUInt(&elementCountParsedOk);

    if(!elementCountParsedOk || !(elementCount > 0))
    {
        qFatal("Error: Element count must be a positive integer");
    }
}

void DirListApp::listElements()
{
    QDir dir(path);
    QDirIterator it(dir, QDirIterator::Subdirectories | QDirIterator::FollowSymlinks);

    while(it.hasNext() && (elementCount-- > 0))
    {
        QFileInfo fi(it.next());

        if(fi.isDir())
        {

            consoleOutput << QString("Directory: %1").arg(fi.path()) << endl;
        }
        else if(fi.isFile())
        {
            consoleOutput << QString("\t%1 kb %2 %3")
                            .arg(fi.size() / 1024, 10)
                            .arg(fi.fileTime(QFileDevice::FileModificationTime).toString("yyyy-MM-dd HH:mm"), 30)
                            .arg(fi.fileName())
                            << endl;
        }
    }
}
